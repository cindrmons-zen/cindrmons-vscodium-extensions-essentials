# Change Log

All notable changes to the "cindrmons-vscodium-extensions-essentials" extension pack will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.0.1] - 2021-11-22

### Added

- Additional Core extensions to be used

### Removed

- Core HTML Extensions (VSCode Pigments, es6-string-html, Live Server, etc...)

## [1.0.0] - 2021-05-31

### Added

- Existing Extensions from old git repo

### Removed

- Framework-Specific Extensions (Vetur, PHP Formatter, Tailwind CSS, etc...)
