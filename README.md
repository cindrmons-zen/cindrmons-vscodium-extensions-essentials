# cindrmons-vscodium-extensions (essentials edition)

Get the pack [here](https://marketplace.visualstudio.com/items?itemName=cindrmon.cindrmons-vscodium-extensions-essentials)!

> My Main essential VSCode/ium extensions.

> Have convenience in using tools like these, for it will help you solve problems faster and help you be more creative in what you do.

## Git Essentials

> Git is a quintissential tool for programmers and devs alike, don't underestimate the power of git. Linus Torvalds is watching...

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
  - Very Important to see what stuff you have done in your code, a must have if you use git a lot (unless you use mercurial or some other version control program, then you should find something else).
- [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
  - If you wanna see your git commits in a graph form (cuz you love to see graphs, I guess), then this tool is the one for you!
- [Gitignore](https://marketplace.visualstudio.com/items?itemName=odezombiech.gitignore)
  - Having a .gitignore file is important, especially if you want to not add files like `node_modules` to your git repo, because, you don't wanna be in _that_ situation @~@

## TODO Essentials

> If you do TODOs a lot, you may need these tools to identify TODO marks

- [Todo Highlight](https://marketplace.visualstudio.com/items?itemName=jgclark.vscode-todo-highlight)
  - This'll highlight all todos, fixmes, bugs, etc, just so it is easily identifiable while you're coding, or you're debugging code of someone else who conveniently uses todos a lot. Just hope that they d use todos.
- [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
  - Besides the highlighter, this tool would organise all your todos, readmes, bugs, etc, into a tree, and you'll definitely see all of them at once!

> This is also a reminder to add todos often in your code, especially if you don't wanna forget adding a particular feature, or fixing a particular bug in your code. This will really help you, and other people as well.

## Code Essentials

> A mish-mash of other tools, in which I have no particular category for them to be put in.

- [Bookmarks](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks)
  - Honestly, I just found this one on the spot as I am making this extension pack, I might figure out how this works, but yea, I guess this is useful in your situation.
- [Indent Rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)
  - Know how many indents you're using in your code with this tool! I also haven't used this, but it is essential if you're using python, or you wanna see how things align if you use too many indents in your code. (But formatting it would make things easier for you.)
- [Bracket Pair Colorizer](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
  - If you're coding, you may have an issue with bracket pairs (unless you're using python). Fear not, this will help you know which bracket pairs are still open, and you won't have errors that will frustrate you for hours finding where each pair goes when typing your code.
- [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
  - There are certain situations wherein builtin path intellisense sometimes doesn't work in some cases, so this might come in handy.
- [File Templates](https://marketplace.visualstudio.com/items?itemName=bam.vscode-file-templates)
  - Tired of typing out boilerplate code for emmet when making a new file? This may be useful for you! This will give you boilerplate code as you are making a file within your project. Talk about convenience.

## Linting

> More convenience awaits you when you use linting, although, more frustration will become of you as well. But stricter guidelines does help in terms of your coding discipline, and that's most important. This is optional for you though, you do you.

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
  - One of the most famous Linters! Goes well with prettier.
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
  - One of the most basic tools for lintiing. Just press `Ctrl+Shift+I` to auto-format your code to how your linter suggests it look like!
- [Status Bar Format Toggle](https://marketplace.visualstudio.com/items?itemName=tombonnike.vscode-status-bar-format-toggle)
  - Don't want to lint all the time? This will get you covered! Turn off auto-frmatting with this tool!

## Compiled Language Support

> This tool is specifically for "still-needs-compiling" scripting languages like typescript, sass/scss, etc, and the best tool for that is.....

- [Hero](https://marketplace.visualstudio.com/items?itemName=Wscats.eno)
  - Auto-compile scripts that need compiling with this tool! Just heard about this right now, but I think this might be helpful for scss/sass!

## Main Theme I Use

- [Monokai Pro](https://marketplace.visualstudio.com/items?itemName=monokai.theme-monokai-pro-vscode)
  - I dunno, I just like the aesthetic of Monokai a lot. Sure, there would be your typical annoying popups just for using this one, but yea, personal preference! You do you, I do mine!

## Main Discord Presence Tool

> I use discord, so yes, I will use this... And I'm mainly gonna use:

- [icrawl's Discord Presence](https://marketplace.visualstudio.com/items?itemName=icrawl.discord-vscode)
  - There are lots out there, this is just one of them, so I am accustomed to how this one works. Anything else works just fine.

## Main Keybinding Tools

> Although, these two don't mix together, but yeah...

- [Vim Keybindings](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim)
  - I like vim, so I use vim btw.
- [Sublime Text Keybindings](https://marketplace.visualstudio.com/items?itemName=ms-vscode.sublime-keybindings)
  - I like sublime text keybindings as well...

## Web Development Essentials

> Main tools for me to use in developing web development

- [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
  - A tool for serving a live version of your html code that auto-updates when you save, so you don't have to refresh every time you open an html file all the time.
- [SCSS Everywhere](https://marketplace.visualstudio.com/items?itemName=gencer.html-slim-scss-css-class-completion)
  - I love SCSS, so this is a good tool if you love SCSS too!
- [FontAwesome AutoComplete](https://marketplace.visualstudio.com/items?itemName=janne252.fontawesome-autocomplete)
  - If you use FontAwesome for your icons, and you use them in your project directly through a cdn, then use this to help you out.
- [VS Code Pigments](https://marketplace.visualstudio.com/items?itemName=jaspernorth.vscode-pigments)
  - Can't visualise what a colour looks like when typing hexcode in your CSS/SCSS style? This'll help you out by showing the exact colour it shows when you type out a particular hexcode (also works with RGB and HSL) colour!
- [Color Picker](https://marketplace.visualstudio.com/items?itemName=anseki.vscode-color)
  - This works really well in tangent t the previous tool mentioned, as this lets you choose a specific colour you want right into VSCode/ium! No need to go to a website for you to select that specific colour you want.
